# vault-monitor-agent #

## Description

Hashicorp's Vault xinetd monitor agent. This component is designed to return
HTTP response codes and additional information through xinetd to report the current
state of a Vault instance.

This component provides Layer7 (httpcheck) checkups to help load balancers (like HAProxy) 
decide whether to route messages to backend instances or not.

## Dependencies ##

* Xinetd properly installed
* TCP Port 9001 open

## Configuration ##

* Place the file **vaultStatus.sh** on a folder and assign execution permissions:

> chmod +x consulStatus.sh

* Edit **vaultStatus.sh** file and update the **VAULT_CONFIG_DIR** variable.

* Create a new xinetd service:

> sudo nano /etc/xinetd.d/vault-status

* Add the following content to the new xinetd service configuration:

```
service vault_monitor
{
        flags = REUSE
        socket_type = stream
        port = 9001
        wait = no
        user = nobody
        server = [ABSOLUTE_PATH_TO_FOLDER]/vaultStatus.sh
        log_on_failure += USERID
        disable = no
        type = UNLISTED
}
```

* Restart xinetd service:

> sudo /etc/init.d/xinetd restart

## Changelog ##

| VERSION       | DESCRIPTION  |
|:-------------:|:-------------|
|1.0.0| Initial Version|
