#!/bin/bash
VAULT_CONFIG_DIR="/home/app-admin/Software/"

listenerTagFound=0
while read line
do
	if [ $listenerTagFound -eq 1 ]
        then
		if [[ "$line" == *"address"* ]];
		then
			address=${line##*=}
			address="$(echo -e "${address}" | tr -d '[:space:],\"')"
			export VAULT_ADDR=http://$address
			#echo $address
		fi
        fi
	if [[ "$line" == *"listener"*  ]];
	then
		listenerTagFound=1
	fi
done < ${VAULT_CONFIG_DIR}/vault-example.hcl

IFS=':'

# vaultStatusFlag defines three possible Vault states:
#	0 -> Unknown
#	1 -> Active
#	2 -> Standby
vaultStatusFlag=0
while read -r parameter  status
do
	if [[ "$parameter" == *"Mode"* ]];
	then
		if [[ "$status" == *"active"*  ]];
		then
			vaultStatusFlag=1
		elif [[ "$status" == *"standby"* ]];
		then
			vaultStatusFlag=2
		fi
	fi
done <<< `vault status`

#echo $vaultStatusFlag

if [ $vaultStatusFlag -eq 0 ]
then
#	echo "Vault status is unknown"
	jsonResponse="{'service-available':'false','vault-status':'unknown'}"
	jsonSize=${#jsonResponse}
	jsonSize=$((jsonSize + 2))
	echo -en  "HTTP/1.1 503 Service Unavailable\r\n"
	echo -en "Content-Type: application/json\r\n"
	echo -en "Connection: close\r\n"
	echo -en "Content-Length: $jsonSize\r\n"
	echo -en "\r\n"
	echo -en "$jsonResponse\r\n"
	exit 1
elif [ $vaultStatusFlag -eq 1 ]
then
#	echo "Vault status is Active"
        jsonResponse="{'service-available':'true','vault-status':'active'}"
        jsonSize=${#jsonResponse}
        jsonSize=$((jsonSize + 2))
	echo -en "HTTP/1.1 200 OK\r\n"
        echo -en "Content-Type: application/json\r\n"
        echo -en "Connection: close\r\n"
        echo -en "Content-Length: $jsonSize\r\n"
        echo -en "\r\n"
        echo -en "$jsonResponse\r\n"
	exit 0
elif [ $vaultStatusFlag -eq 2 ]
then
#	echo "Vault status is Standby"
        jsonResponse="{'service-available':'true','vault-status':'standby'}"
        jsonSize=${#jsonResponse}
        jsonSize=$((jsonSize + 2))
        echo -en "HTTP/1.1 503 Service Unavailable\r\n"
        echo -en "Content-Type: application/json\r\n"
        echo -en "Connection: close\r\n"
        echo -en "Content-Length: $jsonSize\r\n"
        echo -en "\r\n"
        echo -en "$jsonResponse\r\n"
	exit 1
fi
